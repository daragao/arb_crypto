package exchangedata

import (
	. "bitbucket.org/daragao/arb_crypto/xdata"

	"testing"
)

var GdaxL2Snapshot = []byte(`{ "type": "snapshot", "product_id": "BTC-EUR", "bids": [["6500.11", "0.45054140"]], "asks": [["6500.15", "0.57753524"]] }`)

var GdaxL2Update1 = []byte(`{ "type": "l2update", "product_id": "BTC-EUR", "changes": [ ["buy", "6500.09", "0.84702376"], ["sell", "6507.00", "1.88933140"], ["sell", "6505.54", "1.12386524"], ["sell", "6535.222", "1.12386524"], ["sell", "6532.44", "1.12386524"], ["sell", "6504.38", "0"] ] }`)

var GdaxL2Update2 = []byte(`{ "type": "l2update", "product_id": "BTC-EUR", "changes": [ ["sell", "6507.00", "0"] ] }`)

func TestGdaxOrderbook(t *testing.T) {
	gdaxBook := &GdaxBook{Bids: make(map[float64]float64), Asks: make(map[float64]float64)}
	gdaxBook.UnmarshalJSON(GdaxL2Snapshot)
	if gdaxBook.Bids[6500.11] == 0 || len(gdaxBook.Asks) != 1 {
		t.Error("Snapshot badly parsed")
	}
	gdaxBook.UnmarshalJSON(GdaxL2Update1)
	if gdaxBook.Bids[6500.11] == 0 {
		t.Error("removed snapshot data from gdax book")
	}
	if len(gdaxBook.Asks) != 5 {
		t.Error("GdaxL2Update not adding to gdax book", gdaxBook.Asks)
	}
	gdaxBook.UnmarshalJSON(GdaxL2Update2)
	if gdaxBook.Bids[6507.00] != 0 {
		t.Error("did not removed snapshot data")
	}
	// t.Log("Asks", gdaxBook.Asks)
	// t.Log("Asks", gdaxBook.Bids)
}

func TestCommonOrderBook(t *testing.T) {

	var books CommonBook
	gdaxBook := &GdaxBook{Bids: make(map[float64]float64), Asks: make(map[float64]float64)}
	gdaxBook.UnmarshalJSON(GdaxL2Snapshot)
	books.Fill(&gdaxBook.Bids, &gdaxBook.Asks)
	// check if filled book with snapshot
	if (*books.Asks)[6500.15] != 0.57753524 || len((*books.Asks)) != 1 {
		t.Error("Failed to Fill() book with Asks", (*books.Asks))
	}
	if (*books.Bids)[6500.11] != 0.45054140 || len((*books.Bids)) != 1 {
		t.Error("Failed to Fill() book with Asks", (*books.Bids))
	}

	gdaxBook.UnmarshalJSON(GdaxL2Update1)
	// check if filled book with update1
	if (*books.Asks)[6507.00] != 1.88933140 || len((*books.Asks)) != 5 {
		t.Error("Failed to Fill() book with Asks", (*books.Asks))
	}
	if (*books.Bids)[6500.09] != 0.84702376 || len((*books.Bids)) != 2 {
		t.Error("Failed to Fill() book with Asks", (*books.Bids))
	}

	gdaxBook.UnmarshalJSON(GdaxL2Update2)
	// check if filled book with update2
	if (*books.Asks)[6507.00] != 0 || len((*books.Asks)) == 5 {
		t.Error("Failed to Fill() book with Asks", (*books.Asks), (*books.Asks)[6507.00])
	}
	if (*books.Bids)[6507.00] != 0 || len((*books.Bids)) == 1 {
		t.Error("Failed to Fill() book with Asks", (*books.Bids))
	}

}
