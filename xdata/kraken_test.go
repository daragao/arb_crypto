package exchangedata

import (
	"log"
	"testing"

	. "bitbucket.org/daragao/arb_crypto/xdata"
)

func TestGetAssetPairs(t *testing.T) {
	assetPair := GetAssetPairsKraken()
	for key := range assetPair {
		if assetPair[key].Altname == "XBTGBP" {
			log.Printf("%#v\n", assetPair[key])
		}
	}
}
