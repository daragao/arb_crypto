package exchangedata

import (
	"log"
	"math"
	"sort"
	"strconv"
	"sync"
	"time"
)

//MinMaxPrice form book
func minMaxPrice(bids, asks map[float64]float64) (float64, float64) {
	minPrice := math.MaxFloat64
	maxPrice := -math.MaxFloat64
	for price := range asks {
		if price < minPrice {
			minPrice = price
		}
	}
	for price := range bids {
		if price > maxPrice {
			maxPrice = price
		}
	}
	return minPrice, maxPrice
}

func getBestPrices(bids, asks map[float64]float64) ([]float64, []float64) {
	var keysAsks []float64
	for k := range asks {
		keysAsks = append(keysAsks, k)
	}
	var keysBids []float64
	for k := range bids {
		keysBids = append(keysBids, k)
	}
	sort.Float64s(keysAsks)
	sort.Sort(sort.Reverse(sort.Float64Slice(keysBids)))
	return keysAsks, keysBids
}

// CommonBook used for strategy
type CommonBook struct {
	sync.Mutex
	ID         string
	Quote      string
	Base       string
	Bids       map[float64]float64
	Asks       map[float64]float64
	BidKeys    []float64
	AskKeys    []float64
	LastUpdate time.Time
}

// Init commonbook
func (cb *CommonBook) Init(id, quote, base string) {
	cb.Quote = quote
	cb.Base = base
	cb.Bids = make(map[float64]float64)
	cb.Asks = make(map[float64]float64)
	cb.ID = id
}

// IsEmpty checks if it is empty
func (cb *CommonBook) IsEmpty() bool {
	cb.Lock()
	isEmpty := len(cb.Bids) == 0 || len(cb.Asks) == 0 || len(cb.Bids) == 0 || len(cb.Asks) == 0
	cb.Unlock()
	return isEmpty
}

// Print best price
func (cb *CommonBook) Print() {
	if cb.IsEmpty() {
		return
	}
	ask := cb.AskKeys[0]
	bid := cb.BidKeys[0]
	askSize := cb.Asks[ask]
	bidSize := cb.Bids[bid]
	log.Printf("%s:\t ASK(%10.3f x %10.3f = %10.3f)\tBID(%10.3f x %10.3f = %10.3f)\n",
		cb.ID, ask, askSize, ask*askSize, bid, bidSize, bid*bidSize)
}

// UpdateBestPrice the common book
// returns true if the best bid or ask have been updated
func (cb *CommonBook) UpdateBestPrice() bool {
	//askPrice, bidPrice := getBestPrices(bids, asks)
	askPrice, bidPrice := minMaxPrice(cb.Bids, cb.Asks)
	updated := (cb.AskKeys != nil || cb.BidKeys != nil) && (askPrice != cb.AskKeys[0] || bidPrice != cb.BidKeys[0])
	cb.Lock()
	cb.BidKeys = []float64{bidPrice}
	cb.AskKeys = []float64{askPrice}
	cb.LastUpdate = time.Now()
	cb.Unlock()
	return updated
}

func (cb *CommonBook) updateBookPriceStr(side string, priceStr string, sizeStr string) {
	price, err := strconv.ParseFloat(priceStr, 64)
	if err != nil {
		log.Println("ERROR:", err)
		return
	}
	size, err := strconv.ParseFloat(sizeStr, 64)
	if err != nil {
		log.Println("ERROR:", err)
		return
	}
	cb.updateBookPrice(side, price, size)
}

func (cb *CommonBook) updateBookPrice(side string, price float64, size float64) {
	var bookSide map[float64]float64
	if side == "buy" {
		bookSide = cb.Bids
	}
	if side == "sell" {
		bookSide = cb.Asks
	}
	cb.Lock()
	bookSide[price] = size
	if size == 0 {
		delete(bookSide, price)
	}
	cb.Unlock()
}

// ========================================================
// GDAX
// ========================================================

// UnmarshalJSONGdax unmarshal gdax
func (cb *CommonBook) UnmarshalJSONGdax(data []byte) {
	var order GdaxLevel2
	order.UnmarshalJSON(data)
	cb.orderUpdateGdax(order)
}

func (cb *CommonBook) orderUpdateGdax(level2Data GdaxLevel2) {
	if level2Data.Type == "snapshot" {
		for _, ask := range level2Data.Asks {
			cb.updateBookPriceStr("sell", ask[0], ask[1])
		}
		for _, bid := range level2Data.Bids {
			cb.updateBookPriceStr("buy", bid[0], bid[1])
		}
	}
	if level2Data.Type == "l2update" {
		for _, changes := range level2Data.Changes {
			cb.updateBookPriceStr(changes[0], changes[1], changes[2])
		}
	}
	/*
		if level2Data.Type == "heartbeat" {
			// check timestap
			log.Println("GDAX Timestamp:",level2Data.Time, time.Since(level2Data.Time))
		}
	*/
}

// ========================================================
// Coinfloor
// ========================================================

// UnmarshalJSONCoinfloor coinfloorbook
func (cb *CommonBook) UnmarshalJSONCoinfloor(data []byte) {
	var order CoinfloorOrder
	order.UnmarshalJSON(data)

	if order.Notice == "" {
		var snapshot CoinfloorOrderSnapshot
		snapshot.UnmarshalJSON(data)

		for index := range snapshot.Orders {
			snapshot.Orders[index].Base = coinfloorSymbol2ID(cb.Base)
			snapshot.Orders[index].Counter = coinfloorSymbol2ID(cb.Quote)
			snapshot.Orders[index].Notice = "OrderOpened"
			cb.orderUpdateCoinfloor(&snapshot.Orders[index]) // better to use index to avoid value copy
		}
	} else {
		cb.orderUpdateCoinfloor(&order)
	}
}

func (cb *CommonBook) orderUpdateCoinfloor(order *CoinfloorOrder) {
	price := order.Price
	size := order.Quantity
	var bookSide map[float64]float64
	if size < 0 { // sell order
		size = -size
		bookSide = cb.Asks
	} else { // buy order
		bookSide = cb.Bids
	}
	cb.Lock()
	if order.Notice == "OrderClosed" {
		bookSide[price] = bookSide[price] - size
	}
	if order.Notice == "OrderOpened" {
		bookSide[price] = bookSide[price] + size
		//log.Println("Coinfloor Timestamp:",order.Timestamp(),time.Since(order.Timestamp()))
	}
	if bookSide[price] <= 0 {
		delete(bookSide, price)
	}
	cb.Unlock()
}

// ========================================================
// Kraken
// ========================================================

// UnmarshalJSONHitBTC krakenbook
func (cb *CommonBook) UnmarshalJSONHitBTC(data []byte) {
	update := L2UpdateHitBTC{}
	update.UnmarshalJSONHitBTC(data)
	for idx := range update.Bid {
		cb.Bids[update.Bid[idx].Price] = update.Bid[idx].Size
		if update.Bid[idx].Size <= 0 {
			delete(cb.Bids, update.Bid[idx].Price)
		}
	}
	for idx := range update.Ask {
		cb.Asks[update.Ask[idx].Price] = update.Ask[idx].Size
		if update.Ask[idx].Size <= 0 {
			delete(cb.Asks, update.Ask[idx].Price)
		}
	}
}

// ========================================================
// Kraken
// ========================================================

// UnmarshalJSONKraken krakenbook
func (cb *CommonBook) UnmarshalJSONKraken(data []byte, pairKey string) {
	var bookKraken ResponseBookKraken
	bookKraken.UnmarshalJSON(data)
	cb.Lock()
	cb.Bids = make(map[float64]float64)
	cb.Asks = make(map[float64]float64)
	cb.Unlock()
	bids := bookKraken.Result[pairKey].Bids
	asks := bookKraken.Result[pairKey].Asks
	var level2 Level2Kraken
	cb.Lock()
	for _, bid := range bids {
		level2.UpdateLevel2Kraken(bid)
		cb.Bids[level2.Price] = level2.Size
	}
	for _, ask := range asks {
		level2.UpdateLevel2Kraken(ask)
		cb.Asks[level2.Price] = level2.Size
	}
	cb.Unlock()
}

// ========================================================
// Bitfinex
// ========================================================

// UnmarshalJSONBitifinex bitfinexbook
func (cb *CommonBook) UnmarshalJSONBitfinex(data []byte) {
	update := L2UpdateBitfinex{}
	update.UnmarshalJSONBitfinex(data)
	var bookSide *map[float64]float64
	for _, order := range update.Orders {
		if order.Amount > 0 {
			bookSide = &cb.Bids
		} else {
			bookSide = &cb.Asks
		}
		(*bookSide)[order.Price] = math.Abs(order.Amount)
		if order.Count == 0 {
			delete(*bookSide, order.Price)
		}
	}
}
