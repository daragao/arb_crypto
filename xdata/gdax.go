package exchangedata

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	util "bitbucket.org/daragao/arb_crypto/utils"
)

// GdaxBookREST order book response
type GdaxBookREST struct {
	Bids [][]interface{} `json:"bids"`
	Asks [][]interface{} `json:"asks"`
}

// GetGdaxOrderBook API request to get orderbook
func GetGdaxOrderBook(base, counter string) []byte {
	b := util.Get("https://api.gdax.com/products/" + base + "-" + counter + "/book")
	return b
}

// GdaxTicker struct
//{
//	"type": "ticker",
//	"trade_id": 20153558,
//	"sequence": 3262786978,
//	"time": "2017-09-02T17:05:49.250000Z",
//	"product_id": "BTC-USD",
//	"price": "4388.01000000",
//	"side": "buy",
//	// Taker side
//	"last_size": "0.03000000",
//	"best_bid": "4388",
//	"best_ask": "4388.01"
//}
type GdaxTicker struct {
	Type      string    `json:"type"`
	TradeID   int       `json:"trade_id"`
	Sequence  int64     `json:"sequence"`
	Time      time.Time `json:"time"`
	ProductID string    `json:"product_id"`
	Price     string    `json:"price"`
	Side      string    `json:"side"`
	// Taker side
	LastSize string `json:"last_size"`
	BestBid  string `json:"best_bid"`
	BestAsk  string `json:"best_ask"`
}

// UnmarshalJSON Gdax ticker
func (t GdaxTicker) UnmarshalJSON(data []byte) (GdaxTicker, error) {
	if err := json.Unmarshal(data, &t); err != nil {
		return t, err
	}
	return t, nil
}

// GdaxLevel2 struct is the order book
/*
{
	"type": "snapshot",
	"product_id": "BTC-EUR",
	"bids": [["6500.11", "0.45054140"]],
	"asks": [["6500.15", "0.57753524"]]
}
{
	"type": "l2update",
	"product_id": "BTC-EUR",
	"changes": [
	["buy", "6500.09", "0.84702376"],
	["sell", "6507.00", "1.88933140"],
	["sell", "6505.54", "1.12386524"],
	["sell", "6504.38", "0"]
	]
}
*/
type GdaxLevel2 struct {
	Type      string      `json:"type"`
	ProductID string      `json:"product_id"`
	Bids      [][2]string `json:"bids,omitempty,[][]string"`
	Asks      [][3]string `json:"asks,omitempty,[][]string"`
	Changes   [][3]string `json:"changes,omitempty"`
	// Heartbeat
	Sequence    int       `json:"sequence"`
	LastTradeID int       `json:"last_trade_id"`
	Time        time.Time `json:"time"`
}

// UnmarshalJSON Gdax ticker
func (t *GdaxLevel2) UnmarshalJSON(data []byte) {
	if err := json.Unmarshal(data, &t); err != nil {
		log.Println("There was an error:", err)
		return
	}
}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// ----------------------------------------------------------------

type subscribeChannel struct {
	Name       string   `json:"name"`
	ProductIDs []string `json:"product_ids"`
}

type subscribeReq struct {
	Type     string             `json:"type"`
	Channels []subscribeChannel `json:"channels"`
}

// SubscribeReqGdax get subscribe json
func SubscribeReqGdax(quote, base string) string {
	productID := fmt.Sprintf("%s-%s", quote, base)
	chanSubLevel2 := subscribeChannel{"level2", []string{productID}}
	chanSubHeartbeat := subscribeChannel{Name: "heartbeat", ProductIDs: []string{productID}}
	reqSub := subscribeReq{Type: "subscribe", Channels: []subscribeChannel{chanSubLevel2, chanSubHeartbeat}}
	b, err := json.Marshal(&reqSub)
	if err != nil {
		log.Println("Error marshal struct:", err)
	}
	return string(b)
}
