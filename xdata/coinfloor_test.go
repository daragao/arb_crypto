package exchangedata

import (
	. "bitbucket.org/daragao/arb_crypto/xdata"

	"testing"
)

var OrderJSON = []string{
	`{"base":63488,"counter":64032,"id":181707897,"notice":"OrderClosed","price":692800,"quantity":-19876}`,
	`{"base":63488,"counter":64032,"id":181707899,"notice":"OrderOpened","price":693800,"quantity":-19876,"time":1525940247585259}`,
	`{"base":63488,"counter":64032,"id":181707895,"notice":"OrderClosed","price":694000,"quantity":-11859}`,
	`{"base":63488,"counter":64032,"id":181707894,"notice":"OrderClosed","price":692900,"quantity":-2883}`,
	`{"base":63488,"counter":64032,"id":181707893,"notice":"OrderClosed","price":692100,"quantity":-2886}`,
	`{"base":63488,"counter":64032,"id":181707892,"notice":"OrderClosed","price":688100,"quantity":20324}`,
	`{"base":63488,"counter":64032,"id":181707891,"notice":"OrderClosed","price":688800,"quantity":14503}`,
	`{"base":63488,"counter":64032,"id":181707890,"notice":"OrderClosed","price":689500,"quantity":2897}`,
	`{"base":63488,"counter":64032,"id":181707889,"notice":"OrderClosed","price":690000,"quantity":2895}`,
	`{"base":63488,"counter":64032,"id":181707901,"notice":"OrderOpened","price":690000,"quantity":2895,"time":1525940250469863}`,
	`{"base":63488,"counter":64032,"id":181707902,"notice":"OrderOpened","price":689500,"quantity":2897,"time":1525940250470934}`,
	`{"base":63488,"counter":64032,"id":181707903,"notice":"OrderOpened","price":688800,"quantity":14503,"time":1525940250471902}`,
	`{"base":63488,"counter":64032,"id":181707904,"notice":"OrderOpened","price":688100,"quantity":20324,"time":1525940250472870}`,
	`{"base":63488,"counter":64032,"id":181707905,"notice":"OrderOpened","price":692100,"quantity":-2886,"time":1525940250473876}`,
	`{"base":63488,"counter":64032,"id":181707906,"notice":"OrderOpened","price":692900,"quantity":-2883,"time":1525940250474880}`,
	`{"base":63488,"counter":64032,"id":181707907,"notice":"OrderOpened","price":694000,"quantity":-11858,"time":1525940250475883}`,
	`{"base":63488,"counter":64032,"id":181707908,"notice":"OrderOpened","price":677200,"quantity":11200,"time":1525940253372854}`,
	`{"base":63488,"counter":64032,"id":181707908,"notice":"OrderClosed","price":677200,"quantity":11200}`,
	`{"base":63488,"counter":64032,"id":181707901,"notice":"OrderClosed","price":690000,"quantity":2895}`,
}

// last two elements are special because they have floating point
var SnapshotJSON = []byte(`{
	"error_code":0,
	"orders":[
	{"id":181707754,"price":690300,"quantity":19237,"time":1525940191731703},
	{"id":181707781,"price":690300,"quantity":647,"time":1525940200369254},
	{"id":181707741,"price":690200,"quantity":50000,"time":1525940186076502},
	{"id":181707889,"price":690000,"quantity":2895,"time":1525940244731406},
	{"id":181707890,"price":689500,"quantity":2897,"time":1525940244731867},
	{"id":181707798,"price":688900,"quantity":32188,"time":1525940204513815},
	{"id":181707891,"price":688800,"quantity":14503,"time":1525940244732370},
	{"id":179095508,"price":5000000,"quantity":-20000,"time":1525460406063250},
	{"id":180945628,"price":59071800,"quantity":-5925,"time":1525783845811564},
	{"id":179095972,"price":199999800,"quantity":-2461,"time":1525460504271851},
	{"id":179095811,"price":200000000,"quantity":-5618,"time":1525460461140700},
	{"id":180945628,"price":59071800,"quantity":-5925,"time":1525783845811564}
	]
}`)
var WelcomeMsg = []byte(`{"nonce":"UgBOQVgBfLB9SNGNV5En+Q==","notice":"Welcome"}`)
var OrderOpenJSON = []byte(OrderJSON[1])
var OrderClosedJSON = []byte(OrderJSON[0])

func TestCoinfloorOrder(t *testing.T) {
	var order CoinfloorOrder
	order.UnmarshalJSON(OrderOpenJSON)
	// scales price and quantity by 100 and 10000
	if order.Base != 63488 || order.Price != 6938.00 || order.Quantity != -1.9876 || order.Notice != "OrderOpened" {
		t.Error("Bad formed order", order)
	}
	order.UnmarshalJSON(OrderClosedJSON)
	// scales price and quantity by 100 and 10000
	if order.ID != 181707897 || order.Notice != "OrderClosed" {
		t.Error("Bad formed order", order)
	}
}

func TestCoinfloorSnapshot(t *testing.T) {
	var snapshot CoinfloorOrderSnapshot
	snapshot.UnmarshalJSON(SnapshotJSON)

	if len(snapshot.Orders) != 12 {
		t.Error("Snapshot wrong number of orders")
	}

	if snapshot.Orders[0].Price != 6903.00 {
		t.Error("Snapshot bad price unmarshaled", snapshot.Orders[12])
	}
}

func TestCoinfloorBook(t *testing.T) {
	book := &CoinfloorBook{Bids: make(map[float64]float64), Asks: make(map[float64]float64)}
	book.UnmarshalJSON(SnapshotJSON)
	if len(book.Bids) != 6 {
		t.Error("Book length wrong", book)
	}
	if len(book.Asks) != 4 {
		t.Error("Book length wrong", book)
	}
	for _, order := range OrderJSON {
		book.UnmarshalJSON([]byte(order))
	}
	if len(book.Bids) != 5 {
		t.Error("Book length wrong", len(book.Bids))
	}
	if len(book.Asks) != 7 {
		t.Error("Book length wrong", len(book.Asks))
	}
}

// test order book
// test order open and order close for price point in book

/*
func TestGetCoinfloorOrderBook(t *testing.T) {
	b := GetCoinfloorOrderBook("XBT", "GBP")
	coinfloorBook := CoinfloorBookREST{}
	if err := json.Unmarshal(b, &coinfloorBook); err != nil {
		t.Error(err)
	}
	t.Log(coinfloorBook)
}

func TestGetGdaxOrderBook(t *testing.T) {
	b := GetGdaxOrderBook("BTC", "GBP")
	gdaxBook := GdaxBookREST{}
	if err := json.Unmarshal(b, &gdaxBook); err != nil {
		t.Error(err)
	}
	t.Log(gdaxBook)
}
*/
