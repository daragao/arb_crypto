package exchangedata

import (
	"encoding/json"
	"fmt"
	"log"
)

// L2OrderBitfinex struct
type L2OrderBitfinex struct {
	Price  float64
	Count  int
	Amount float64
}

// L2UpdateBitfinex struct
type L2UpdateBitfinex struct {
	ChannelID int
	Orders    []L2OrderBitfinex
}

// UnmarshalJSONBitfinex ws bytes to Update HitBTC
func (u *L2UpdateBitfinex) UnmarshalJSONBitfinex(b []byte) {
	var orderData []interface{}
	data := []interface{}{&u.ChannelID, &orderData} // missing alll the orders
	// spaghetti code to verify type!!!!
	// TODO: lease fix this!
	if err := json.Unmarshal(b, &data); err != nil {
		var subRes subscribeChannelBitfinex
		errSubRes := json.Unmarshal(b, &subRes);
		var hb string
		heartbeat := []interface{}{&u.ChannelID, &hb}
		errHB := json.Unmarshal(b, &heartbeat)
		if errSubRes != nil && errHB != nil {
			log.Printf("There was an error: %s\n -----------\n%s\n -----------\n", err, b)
			return
		}
		return
	}

	var orders []L2OrderBitfinex
	if price, ok := orderData[0].(float64); ok {
		count := orderData[1].(float64)
		amount := orderData[2].(float64)
		orders = append(orders, L2OrderBitfinex{Price: price, Count: int(count), Amount: amount})
	} else {
		for _, o := range orderData {
			oArr := o.([]interface{})
			price := oArr[0].(float64)
			count := oArr[1].(float64)
			amount := oArr[2].(float64)
			orders = append(orders, L2OrderBitfinex{Price: price, Count: int(count), Amount: amount})
		}
	}
	u.Orders = orders

}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// ----------------------------------------------------------------

type subscribeChannelBitfinex struct {
	Event   string `json:"event"`
	Channel string `json:"channel"`
	ChanID  int    `json:"chanId,omitempty"`
	Symbol string `json:"symbol"`
	Prec   string `json:"prec"`
	Freq   string `json:"freq"`
	Len    int    `json:"len,string"`
	Pair    string `json:"pair,omitempty"`
}

// SubscribeReqBitfinex get subscribe json
func SubscribeReqBitfinex(quote, base string) string {
	symbol := fmt.Sprintf("t%s%s", quote, base)
	reqSub := subscribeChannelBitfinex{
		Event:   "subscribe",
		Channel: "book",
		Symbol:  symbol,
		Prec:    "P0",
		Freq:    "F0",
		Len:     25,
	}
	b, err := json.Marshal(&reqSub)
	if err != nil {
		log.Println("Error marshal struct:", err)
	}
	return string(b)
}
