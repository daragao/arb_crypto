package exchangedata

import (
	"encoding/json"
	"log"
	"time"

	util "bitbucket.org/daragao/arb_crypto/utils"
)

// CoinfloorOrder struct
type CoinfloorOrder struct {
	Base     int64   `json:"base,omitempty"`
	Counter  int64   `json:"counter,omitempty"`
	ID       int64   `json:"id"`
	Notice   string  `json:"notice,omitempty"`
	Price    float64 `json:"price"`    // if XBT:GBP scale 100
	Quantity float64 `json:"quantity"` // if XBT scale 10000
	Time     int64   `json:"time"`
}

// UnmarshalJSON Coinfloor order
func (t *CoinfloorOrder) UnmarshalJSON(data []byte) {
	if err := json.Unmarshal(data, &t); err != nil {
		log.Println("There was an error:", err)
		return
	}
	// TODO: Make scale variable
	t.Price = t.Price / 100.00         // divide by scale
	t.Quantity = t.Quantity / 10000.00 // divide by scale
}

// Timestamp get time
func (t *CoinfloorOrder) Timestamp() time.Time {
	return time.Unix(t.Time/int64(time.Millisecond), 0)
}

// CoinfloorOrderSnapshot struct
type CoinfloorOrderSnapshot struct {
	ErrorCode int              `json:"error_code"`
	Orders    []CoinfloorOrder `json:"orders"`
}

// UnmarshalJSON Coinfloor order
func (t *CoinfloorOrderSnapshot) UnmarshalJSON(data []byte) {
	if err := json.Unmarshal(data, &t); err != nil {
		log.Println("There was an error:", err)
		return
	}
	// TODO: Make scale variable
	for index := range t.Orders {
		t.Orders[index].Price = t.Orders[index].Price / 100.00         // divide by scale
		t.Orders[index].Quantity = t.Orders[index].Quantity / 10000.00 // divide by scale
	}
}

// CoinfloorBookREST order book response
type CoinfloorBookREST struct {
	Bids [][]interface{} `json:"bids"`
	Asks [][]interface{} `json:"asks"`
}

// GetCoinfloorOrderBook API request to get orderbook
func GetCoinfloorOrderBook(base, counter string) []byte {
	b := util.Get("https://webapi.coinfloor.co.uk:8090/bist/" + base + "/" + counter + "/order_book/")
	return b
}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// ----------------------------------------------------------------

func coinfloorSymbol2ID(symbol string) int64 {
	symbolMap := map[string]int64{"BTC": 64032, "GBP": 63488, "USD": 64128, "EUR": 64000}
	return symbolMap[symbol]
}

type subscribeChannelCoinfloor struct {
	Method  string `json:"method"`
	Base    int64  `json:"base"`
	Counter int64  `json:"counter"`
	Watch   bool   `json:"watch"`
}

// SubscribeReqCoinfloor get subscribe json
func SubscribeReqCoinfloor(quote, base string) string {
	reqSub := subscribeChannelCoinfloor{"WatchOrders", coinfloorSymbol2ID(base), coinfloorSymbol2ID(quote), true}
	b, err := json.Marshal(&reqSub)
	if err != nil {
		log.Println("Error marshal struct:", err)
	}
	return string(b)
}
