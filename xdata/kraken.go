package exchangedata

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	util "bitbucket.org/daragao/arb_crypto/utils"
)

// ResponseAssetPairKraken generic struct
type ResponseAssetPairKraken struct {
	Error  []interface{}              `json:"error"`
	Result map[string]AssetPairKraken `json:result`
}

// AssetPairKraken struct
type AssetPairKraken struct {
	Altname           string        `json:"altname"`
	AclassBase        string        `json:"aclass_base"`
	Base              string        `json:"base"`
	AclassQuote       string        `json:"aclass_quote"`
	Quote             string        `json:"quote"`
	Lot               string        `json:"lot"`
	PairDecimals      int           `json:"pair_decimals"`
	LotDecimals       int           `json:"lot_decimals"`
	LotMultiplier     int           `json:"lot_multiplier"`
	LeverageBuy       []interface{} `json:"leverage_buy"`
	LeverageSell      []interface{} `json:"leverage_sell"`
	Fees              [][]float64   `json:"fees"`
	FeesMaker         [][]float64   `json:"fees_maker"`
	FeeVolumeCurrency string        `json:"fee_volume_currency"`
	MarginCall        int           `json:"margin_call"`
	MarginStop        int           `json:"margin_stop"`
}

// GetAssetPairsKraken from kraken
func GetAssetPairsKraken() map[string]AssetPairKraken {
	data := util.Get("https://api.kraken.com/0/public/AssetPairs")
	//var res Response
	var res ResponseAssetPairKraken
	if err := json.Unmarshal(data, &res); err != nil {
		fmt.Println("There was an error:", err)
		return nil
	}
	return res.Result
}

// ResponseBookKraken request response
type ResponseBookKraken struct {
	Error  []interface{}         `json:"error"`
	Result map[string]BookKraken `json:"result"`
}

// UnmarshalJSON response book kraken
func (r *ResponseBookKraken) UnmarshalJSON(data []byte) {
	if err := json.Unmarshal(data, &r); err != nil {
		fmt.Println("There was an error:", err)
		return
	}
}

// BookKraken struct
type BookKraken struct {
	Asks [][]interface{} `json:"asks"`
	Bids [][]interface{} `json:"bids"`
}

// UnmarshalJSON book kraken
func (b *BookKraken) UnmarshalJSON(data []byte) {
	if err := json.Unmarshal(data, &b); err != nil {
		fmt.Println("There was an error:", err)
		return
	}
}

// Level2Kraken struct
type Level2Kraken struct {
	Price float64
	Size  float64
	Time  time.Time
}

// UpdateLevel2Kraken book kraken
func (l *Level2Kraken) UpdateLevel2Kraken(data []interface{}) {
	l.Price, _ = strconv.ParseFloat(data[0].(string), 64)
	l.Size, _ = strconv.ParseFloat(data[1].(string), 64)
	l.Time = time.Unix(int64(data[2].(float64)), 0)
}
