package exchangedata

import (
	"testing"

	. "bitbucket.org/daragao/arb_crypto/xdata"
)

func TestUnmarshalHitBTC(t *testing.T) {
	result := []string{`{"jsonrpc":"2.0","method":"snapshotOrderbook","params":{"ask":[{"price":"0.054588","size":"0.245"},{"price":"0.054590","size":"0.000"},{"price":"0.054591","size":"2.784"}],"bid":[{"price":"0.054558","size":"0.500"},{"price":"0.054557","size":"0.076"},{"price":"0.054524","size":"7.725"}],"symbol":"ETHBTC","sequence":8073827}}`, `{"jsonrpc":"2.0","method":"snapshotOrderbook","params":{"ask":[{"price":"0.054588","size":"0"},{"price":"0.054590","size":"0.000"},{"price":"0.054591","size":"0.0"}],"bid":[{"price":"0.054558","size":"0"},{"price":"0.054557","size":"0"},{"price":"0.054524","size":"0"},{"price":"0.4","size":"0.00123"}],"symbol":"ETHBTC","sequence":8073827}}`}

	var book CommonBook
	book.Init("HitBTC", "ETH", "BTC")
	book.UnmarshalJSONHitBTC([]byte(result[0]))
	if len(book.Bids) != 3 {
		t.Error("Wrong amount of bids parsed")
	}
	book.UnmarshalJSONHitBTC([]byte(result[1]))
	if len(book.Bids) != 1 {
		t.Error("Wrong amount of bids parsed")
	}
	if book.Asks[0.4] == 0.00123 {
		t.Error("Wrong size")
	}
}

func TestUnmarshalBitfinex(t *testing.T) {
	result := []string{
		`[ 3082, [ [ 8557.6, 2, 0.91999999 ], [ 8557.5, 1, 0.30155739 ], [ 8557.4, 2, 1.5 ], [ 8556.8, 1, 0.90001906 ], [ 8556.5, 1, 0.07012991 ], [ 8556.2, 1, 0.022 ], [ 8555.9, 1, 0.05 ], [ 8555.4, 2, 2.91859999 ], [ 8555.2, 2, 2.917 ], [ 8555.1, 1, 0.48763186 ], [ 8555, 1, 0.15177135 ], [ 8554.6, 1, 0.0866 ], [ 8553.6, 1, 0.3 ], [ 8553.5, 1, 2 ], [ 8551.8, 1, 0.5 ], [ 8551.7, 1, 0.1 ], [ 8551.5, 1, 0.02177034 ], [ 8551.1, 1, 0.1 ], [ 8550.2, 3, 3.325 ], [ 8550, 2, 2.05 ], [ 8549.7, 1, 9.33 ], [ 8549.2, 1, 0.02 ], [ 8548.6, 1, 3 ], [ 8546.8, 1, 0.5 ], [ 8546.2, 1, 10.73 ], [ 8557.7, 2, -0.932889 ], [ 8558, 1, -2 ], [ 8560.3, 2, -1.23309292 ], [ 8560.4, 1, -0.17592019 ], [ 8560.8, 1, -0.06320661 ], [ 8561.1, 1, -0.71608 ], [ 8561.9, 2, -0.90603043 ], [ 8562.4, 1, -0.1 ], [ 8562.6, 1, -1.1663 ], [ 8563.8, 1, -5.8 ], [ 8563.9, 1, -0.5 ], [ 8564.8, 3, -1.84019857 ], [ 8564.9, 1, -3 ], [ 8565, 1, -10 ], [ 8565.6, 1, -0.5 ], [ 8565.7, 1, -2.5 ], [ 8568.4, 1, -0.3 ], [ 8568.5, 1, -0.5 ], [ 8568.7, 1, -8.5 ], [ 8569, 1, -0.5 ], [ 8571.3, 1, -4.73 ], [ 8572.1, 1, -0.5 ], [ 8572.3, 1, -10.8 ], [ 8573.5, 1, -0.3 ], [ 8573.7, 1, -2 ] ] ]`,
		`[ 3082, [ 8573.9, 1, -0.05782715 ] ]`,
		`[ 3082, [ 8562.3, 1, -0.8 ] ]`,
		`[ 3082, [ 8550.2, 1, 2 ] ]`,
		`[ 3082, [ 8550.4, 1, 0.475 ] ]`,
	}

	var book CommonBook
	book.Init("Bitfinex", "ETH", "BTC")
	book.UnmarshalJSONBitfinex([]byte(result[3]))
	if len(book.Bids) != 1 {
		t.Error("Wrong amount of bids parsed")
	}
	book.UnmarshalJSONBitfinex([]byte(result[1]))
	if len(book.Asks) != 1 {
		t.Error("Wrong amount of Asks parsed")
	}
	book.UnmarshalJSONBitfinex([]byte(result[0]))
	if book.Bids[8557.4] != 1.5 {
		t.Error("Wrong size")
	}
	if len(book.Asks) != 26 {
		t.Error("Wrong amount of Asks parsed",len(book.Asks))
	}
}
