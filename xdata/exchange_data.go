package exchangedata

import (
	"os"
	"os/signal"

	"bitbucket.org/daragao/arb_crypto/utils"
)

// BitMex https://www.bitmex.com/app/wsAPI
// Bitfinex https://docs.bitfinex.com/docs/ws-general
// BitStamp https://www.bitstamp.net/websocket/

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

type unmarshalJSON func([]byte)

// TODO: update book only one  level/order at a time

// bookLoop generice book updater
func bookLoop(addr, path, subscribeJSON string, book *CommonBook, updateBook unmarshalJSON) <-chan *CommonBook {

	conn := util.WsConnect(addr, path)
	util.WsWriteMessage(conn, subscribeJSON)
	msg := make(chan []byte)
	go util.WsReadLoop(conn, msg)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	chanBook := make(chan *CommonBook, 10)

	go func() {
		defer close(msg)
		defer conn.Close()
		for {
			select {
			case b := <-msg:
				updateBook(b)
				if book.UpdateBestPrice() { // only update when there is a new best price!
					chanBook <- book
				}
			case <-c:
				return
			}
		}
	}()
	return chanBook
}

// BookLoopGdax loop and update book
// BTC GBP
func BookLoopGdax(counter, base string) <-chan *CommonBook {
	subscribeJSON := SubscribeReqGdax(counter, base)
	var book CommonBook
	book.Init("GDAX", counter, base)
	unmarshal := book.UnmarshalJSONGdax
	url := "ws-feed.gdax.com"
	path := "/"
	bookChan := bookLoop(url, path, subscribeJSON, &book, unmarshal)
	return bookChan
}

// BookLoopCoinfloor loop and update book
// BTC GBP
func BookLoopCoinfloor(counter, base string) <-chan *CommonBook {
	subscribeJSON := SubscribeReqCoinfloor(counter, base)
	var book CommonBook
	book.Init("Coinfloor", counter, base)
	unmarshal := book.UnmarshalJSONCoinfloor
	url := "api.coinfloor.co.uk"
	path := "/"
	bookChan := bookLoop(url, path, subscribeJSON, &book, unmarshal)
	return bookChan
}

// BookLoopHitBTC loop and update book
// BTC USD
func BookLoopHitBTC(counter, base string) <-chan *CommonBook {
	subscribeJSON := SubscribeReqHitBTC(counter, base)
	var book CommonBook
	book.Init("HitBTC", counter, base)
	unmarshal := book.UnmarshalJSONHitBTC
	url := "api.hitbtc.com"
	path := "/api/2/ws"
	bookChan := bookLoop(url, path, subscribeJSON, &book, unmarshal)
	return bookChan
}

// BookLoopBitfinex loop and update book
// BTC USD
func BookLoopBitfinex(counter, base string) <-chan *CommonBook {
	subscribeJSON := SubscribeReqBitfinex(counter, base)
	var book CommonBook
	book.Init("Bitfinex", counter, base)
	unmarshal := book.UnmarshalJSONBitfinex
	url := "api.bitfinex.com"
	path := "/ws/2"
	bookChan := bookLoop(url, path, subscribeJSON, &book, unmarshal)
	return bookChan
}
