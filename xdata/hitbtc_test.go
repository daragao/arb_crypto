package exchangedata

import (
	"testing"

	. "bitbucket.org/daragao/arb_crypto/xdata"
)

func TestSubscribeReq(t *testing.T) {
	result := `{"method":"subscribeOrderbook","params":{"symbol":"ETHBTC"},"id":123}`
	value := SubscribeReqHitBTC("ETH", "BTC")
	if result != value {
		t.Error("Bad subscribe request: \n", value, "\n != \n", result)
	}
}

func TestUnmarshalJSON(t *testing.T) {
	result := `{"jsonrpc":"2.0","method":"snapshotOrderbook","params":{"ask":[{"price":"0.054588","size":"0.245"},{"price":"0.054590","size":"0.000"},{"price":"0.054591","size":"2.784"}],"bid":[{"price":"0.054558","size":"0.500"},{"price":"0.054557","size":"0.076"},{"price":"0.054524","size":"7.725"}],"symbol":"ETHBTC","sequence":8073827}}`

	value := L2UpdateHitBTC{}
	value.UnmarshalJSONHitBTC([]byte(result))
	if value.Sequence != 8073827 {
		t.Error("Badly parsed sequence number")
	}
	if value.Symbol != "ETHBTC" {
		t.Error("Badly parsed symbol")
	}
	if len(value.Ask) != 3 {
		t.Error("Wrong amount of asks parsed")
	}
	if value.Bid[2].Price != 0.054524 {
		t.Error("Wrong price of bid parsed", value.Bid[2].Price)
	}
}

func TestUnmarshalCommonBook(t *testing.T) {
	result := []string{`{"jsonrpc":"2.0","method":"snapshotOrderbook","params":{"ask":[{"price":"0.054588","size":"0.245"},{"price":"0.054590","size":"0.000"},{"price":"0.054591","size":"2.784"}],"bid":[{"price":"0.054558","size":"0.500"},{"price":"0.054557","size":"0.076"},{"price":"0.054524","size":"7.725"}],"symbol":"ETHBTC","sequence":8073827}}`, `{"jsonrpc":"2.0","method":"snapshotOrderbook","params":{"ask":[{"price":"0.054588","size":"0"},{"price":"0.054590","size":"0.000"},{"price":"0.054591","size":"0.0"}],"bid":[{"price":"0.054558","size":"0"},{"price":"0.054557","size":"0"},{"price":"0.054524","size":"0"},{"price":"0.4","size":"0.00123"}],"symbol":"ETHBTC","sequence":8073827}}`}

	var book CommonBook
	book.Init("HitBTC", "ETH", "BTC")
	book.UnmarshalJSONHitBTC([]byte(result[0]))
	if len(book.Bids) != 3 {
		t.Error("Wrong amount of bids parsed")
	}
	book.UnmarshalJSONHitBTC([]byte(result[1]))
	if len(book.Bids) != 1 {
		t.Error("Wrong amount of bids parsed")
	}
	if book.Asks[0.4] == 0.00123 {
		t.Error("Wrong size")
	}
}
