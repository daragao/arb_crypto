package exchangedata

import (
	"encoding/json"
	"fmt"
	"log"
)

// L2OrderHitBTC struct
type L2OrderHitBTC struct {
	Price float64 `json:"price,string"`
	Size  float64 `json:"size,string"`
}

// L2UpdateHitBTC struct
type L2UpdateHitBTC struct {
	Ask      []L2OrderHitBTC `json:"ask"`
	Bid      []L2OrderHitBTC `json:"bid"`
	Symbol   string          `json:"symbol"`
	Sequence int             `json:"sequence"`
}

// UnmarshalJSONHitBTC ws bytes to Update HitBTC
func (u *L2UpdateHitBTC) UnmarshalJSONHitBTC(b []byte) {
	data := struct {
		Jsonrpc string         `json:"jsonrpc"`
		Method  string         `json:"method"`
		Params  L2UpdateHitBTC `json:"params"`
	}{}

	if err := json.Unmarshal(b, &data); err != nil {
		log.Println("There was an error:", err)
		return
	}
	*u = data.Params
}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// ----------------------------------------------------------------

type subscribeChannelHitBTC struct {
	Method string `json:"method"`
	Params struct {
		Symbol string `json:"symbol"`
	} `json:"params"`
	ID int `json:"id"`
}

// SubscribeReqHitBTC get subscribe json
func SubscribeReqHitBTC(quote, base string) string {
	symbol := fmt.Sprintf("%s%s", quote, base)
	reqSub := subscribeChannelHitBTC{Method: "subscribeOrderbook", ID: 123}
	reqSub.Params.Symbol = symbol
	b, err := json.Marshal(&reqSub)
	if err != nil {
		log.Println("Error marshal struct:", err)
	}
	return string(b)
}
