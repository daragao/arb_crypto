package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"bitbucket.org/daragao/arb_crypto/strategy"
	"bitbucket.org/daragao/arb_crypto/xdata"
)

func printBooks(books []*exchangedata.CommonBook) {
	for idx := range books {
		fmt.Printf("%10s len(Asks,Bids) %4d %4d Best(Ask: %8.3f,%8.3f Bid: %8.3f,%8.3f) \t",
			books[idx].ID,
			len(books[idx].Asks),
			len(books[idx].Bids),
			books[idx].AskKeys[0],
			books[idx].Asks[books[idx].AskKeys[0]],
			books[idx].BidKeys[0],
			books[idx].Bids[books[idx].BidKeys[0]])
	}
	fmt.Printf("\r")
}

// curl -s "http://free.currencyconverterapi.com/api/v5/convert?q=EUR_USD,USD_GBP&compact=n" | jq .
func loopMarket(quote, base string) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	books := make([]*exchangedata.CommonBook, 2)
	gdaxBookChan := exchangedata.BookLoopGdax(quote, base)
	coinfloorBookChan := exchangedata.BookLoopCoinfloor(quote, base)
	books[0] = <-gdaxBookChan
	books[1] = <-coinfloorBookChan
	booksChan := make(chan []*exchangedata.CommonBook, 1)
	go strategy.PrintSpread(booksChan)

	for {
		select {
		case <-gdaxBookChan:
			//log.Printf("%10s %4d %4d %6f %6f\n", books[0].ID, len(books[0].Asks), len(books[0].Bids), books[0].AskKeys, books[0].BidKeys)
		case <-coinfloorBookChan:
			//log.Printf("%10s %4d %4d %6f %6f\n", books[1].ID, len(books[1].Asks), len(books[1].Bids), books[1].AskKeys, books[1].BidKeys)
		case <-c:
			return
		}
		//printBooks(books)
		booksChan <- books
	}
}

func loopMarketBTCUSD() {
	quote := "BTC"
	base := "USD"
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	books := make([]*exchangedata.CommonBook, 2)
	hitBTCBookChan := exchangedata.BookLoopHitBTC(quote, base)
	books[0] = <-hitBTCBookChan
	bitfinexBookChan := exchangedata.BookLoopBitfinex(quote, base)
	books[1] = <-bitfinexBookChan

	for {
		select {
		case <-hitBTCBookChan:
		case <-bitfinexBookChan:
		case <-c:
			return
		}
		printBooks(books)
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// loopMarket("BTC", "GBP")
	loopMarketBTCUSD()
}
