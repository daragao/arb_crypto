package main

import (
	. "bitbucket.org/daragao/arb_crypto/xdata"

	"testing"
)

// TODO: Bitfinex and Kraken ticker UnmarshalJSON()

func TestJsonParser(t *testing.T) {
	data := `{ "error": [], "result": { "XXBTZEUR": { "a": [ "7625.00000", "3", "3.000" ], "b": [ "7622.70000", "2", "2.000" ], "c": [ "7624.00000", "0.11973464" ], "v": [ "2122.28765227", "4935.89366154" ], "p": [ "7596.63607", "7522.37330" ], "t":  [ 13720, 26784 ], "l": [ "7480.00000", "7334.10000" ], "h": [ "7655.50000", "7655.50000" ], "o": "7541.10000" } } }`
	ParseJSON(data)
}

func TestGdaxParser(t *testing.T) {
	data := []byte(`{
		"type": "ticker",
		"trade_id": 20153558,
		"sequence": 3262786978,
		"time": "2017-09-02T17:05:49.250000Z",
		"product_id": "BTC-USD",
		"price": "4388.01000000",
		"side": "buy",
		"last_size": "0.03000000",
		"best_bid": "4388",
		"best_ask": "4388.01"
	}`)
	t.Log("test")
	ticker, err := GdaxTicker{}.UnmarshalJSON(data)
	if err != nil {
		t.Error(err)
	}
	t.Log(ticker.Type)

	if ticker.Type != "ticker" {
		t.Error("Bad property")
	}
	if ticker.TradeID != 20153558 {
		t.Error("Bad property")
	}
	if ticker.Sequence != 3262786978 {
		t.Error("Bad property")
	}
	//if ticker.Time != "2017-09-02T17:05:49.250000Z" {
	//	t.Error("Bad property")
	//}
	if ticker.ProductID != "BTC-USD" {
		t.Error("Bad property")
	}
	if ticker.Price != "4388.01000000" {
		t.Error("Bad property")
	}
	if ticker.Side != "buy" {
		t.Error("Bad property")
	}
	if ticker.LastSize != "0.03000000" {
		t.Error("Bad property")
	}
	if ticker.BestBid != "4388" {
		t.Error("Bad property")
	}
	if ticker.BestAsk != "4388.01" {
		t.Error("Bad property")
	}

}

func TestPoloniexParser(t *testing.T) {
	data := []byte(`[
	"BTC_BBR",
	"0.00069501",
	"0.00074346",
	"0.00069501",
	"-0.00742634",
	"8.63286802",
	"11983.47150109",
	0,
	"0.00107920",
	"0.00045422"
	]`)
	t.Log("test")
	ticker, err := PoloniexTicker{}.UnmarshalJSON(data)
	if err != nil {
		t.Error(err)
	}
	t.Log(ticker.CurrencyPair)

	if ticker.CurrencyPair != "BTC_BBR" {
		t.Error("Bad property")
	}
	if ticker.Last != "0.00069501" {
		t.Error("Bad property")
	}
	if ticker.LowestAsk != "0.00074346" {
		t.Error("Bad property")
	}
	if ticker.HighestBid != "0.00069501" {
		t.Error("Bad property")
	}
	if ticker.PercentChange != "-0.00742634" {
		t.Error("Bad property", ticker.PercentChange)
	}
	if ticker.BaseVolume != "8.63286802" {
		t.Error("Bad property")
	}
	if ticker.QuoteVolume != "11983.47150109" {
		t.Error("Bad property")
	}
	if ticker.IsFrozen != 0 {
		t.Error("Bad property")
	}
	if ticker.L24hrHigh != "0.00107920" {
		t.Error("Bad property")
	}
	if ticker.L24hrLow != "0.00045422" {
		t.Error("Bad property")
	}
}
