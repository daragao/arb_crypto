package strategy

import (
	"testing"

	"bitbucket.org/daragao/arb_crypto/strategy"
	"bitbucket.org/daragao/arb_crypto/xdata"
)

var OrderJSON = []string{
	`{"base":63488,"counter":64032,"id":181707897,"notice":"OrderClosed","price":692800,"quantity":-19876}`,
	`{"base":63488,"counter":64032,"id":181707899,"notice":"OrderOpened","price":693800,"quantity":-19876,"time":1525940247585259}`,
	`{"base":63488,"counter":64032,"id":181707901,"notice":"OrderOpened","price":690000,"quantity":2895,"time":1525940250469863}`,
	`{"base":63488,"counter":64032,"id":181707902,"notice":"OrderOpened","price":689500,"quantity":2897,"time":1525940250470934}`,
	`{"base":63488,"counter":64032,"id":181707903,"notice":"OrderOpened","price":688800,"quantity":14503,"time":1525940250471902}`,
	`{"base":63488,"counter":64032,"id":181707904,"notice":"OrderOpened","price":688100,"quantity":20324,"time":1525940250472870}`,
	`{"base":63488,"counter":64032,"id":181707905,"notice":"OrderOpened","price":692100,"quantity":-2886,"time":1525940250473876}`,
	`{"base":63488,"counter":64032,"id":181707906,"notice":"OrderOpened","price":692900,"quantity":-2883,"time":1525940250474880}`,
	`{"base":63488,"counter":64032,"id":181707907,"notice":"OrderOpened","price":694000,"quantity":-11858,"time":1525940250475883}`,
	`{"base":63488,"counter":64032,"id":181707908,"notice":"OrderOpened","price":677200,"quantity":11200,"time":1525940253372854}`,
	`{"base":63488,"counter":64032,"id":181707908,"notice":"OrderClosed","price":677200,"quantity":11200}`,
	`{"base":63488,"counter":64032,"id":181707901,"notice":"OrderClosed","price":690000,"quantity":2895}`,
}

var GdaxL2Update1 = []byte(`{ "type": "l2update", "product_id": "BTC-EUR", "changes": [ ["buy", "6500.09", "0.84702376"], ["sell", "6507.00", "1.88933140"], ["sell", "6505.54", "1.12386524"], ["sell", "6535.222", "1.12386524"], ["sell", "6532.44", "1.12386524"], ["sell", "6504.38", "0"] ] }`)

func TestBasicStrategy(t *testing.T) {
	books := make([]exchangedata.CommonBook, 2)

	gdaxBook := &exchangedata.GdaxBook{Bids: make(map[float64]float64), Asks: make(map[float64]float64)}
	gdaxBook.UnmarshalJSON([]byte(GdaxL2Update1))
	books[0].ID = "GDAX"
	books[0].Fill(&gdaxBook.Bids, &gdaxBook.Asks)
	coinfloorBook := &exchangedata.CoinfloorBook{Bids: make(map[float64]float64), Asks: make(map[float64]float64)}
	for _, order := range OrderJSON {
		coinfloorBook.UnmarshalJSON([]byte(order))
	}
	books[1].ID = "Coinfloor"
	books[1].Fill(&coinfloorBook.Bids, &coinfloorBook.Asks)

	//(*books[0].Bids)[20.0] = 123
	//strategy.RunStrategy(&books)
	bestAsk, bestBid := strategy.BestSpread(&books, "BASE", "COUNTER")

	/*
		for index := range books {
			book := books[index]
			t.Log(book.ID, (*book.Asks), (*book.Bids))
		}
	*/
	bestBid.Print()
	bestAsk.Print()
	t.Log("ASK ", bestAsk, "Bid ", bestBid)
}
