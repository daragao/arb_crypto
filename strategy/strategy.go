package strategy

import (
	"fmt"
	"log"
	"math"
	"time"

	"bitbucket.org/daragao/arb_crypto/xdata"
)

// Order struct
type Order struct {
	Side       string
	Size       float64
	Price      float64
	ExchangeID string
	Base       string
	Counter    string
	LastUpdate time.Time
}

// Print order
func (o *Order) Print() {
	log.Printf("%s %9s Price: %7.3f Size: %6.3f %s/%s (last book update %s)\n",
		o.Side,
		o.ExchangeID,
		o.Price,
		o.Size,
		o.Base,
		o.Counter, time.Since(o.LastUpdate))
}

// GetHash "hash" of the size and price
func (o *Order) GetHash() string {
	return fmt.Sprintf("%s %9s Price: %7.3f Size: %6.3f %s/%s",
		o.Side,
		o.ExchangeID,
		o.Price,
		o.Size,
		o.Base,
		o.Counter)
}

// BestSpread find the spread between books
func BestSpread(books []*exchangedata.CommonBook, base, counter string) (Order, Order) {
	bestBid := Order{Price: 0, Side: "BID", Base: base, Counter: counter}
	bestAsk := Order{Price: math.MaxFloat64, Side: "ASK", Base: base, Counter: counter}
	for index := range books {
		book := books[index]
		if book.IsEmpty() {
			continue
		}
		book.Lock()
		if bestBid.Price < book.BidKeys[0] {
			order := &bestBid
			order.ExchangeID = book.ID
			order.Price = book.BidKeys[0]
			order.Size = book.Bids[order.Price]
			order.LastUpdate = book.LastUpdate
		}
		if bestAsk.Price > book.AskKeys[0] {
			order := &bestAsk
			order.ExchangeID = book.ID
			order.Price = book.AskKeys[0]
			order.Size = book.Asks[order.Price]
			order.LastUpdate = book.LastUpdate
		}
		book.Unlock()
	}
	return bestAsk, bestBid
}

// PrintSpread just print
func PrintSpread(bookChan chan []*exchangedata.CommonBook) {
	minProfit := 0.0
	var lastCheckpoint string
	positionClosed := true
	var positionOpenedTime time.Time
	for {
		select {
		case books := <-bookChan:
			bestAsk, bestBid := BestSpread(books, "GBP", "BTC") // actual strategy

			profit := OrderDifference(&bestBid, &bestAsk)
			checkpoint := bestAsk.GetHash() + bestBid.GetHash()
			if profit > minProfit && checkpoint != lastCheckpoint {
				if positionClosed {
					fmt.Println("=============")
					positionOpenedTime = time.Now()
				}
				bestBid.Print()
				bestAsk.Print()
				log.Printf("Difference of %6.3f GBP", profit)
				lastCheckpoint = checkpoint
				positionClosed = false
			} else {
				if !positionClosed && checkpoint != lastCheckpoint {
					// show highest price and lowest price and change in size
					duration := time.Since(positionOpenedTime)
					log.Printf("Total time while trade in profit (%s)", duration)
					fmt.Println("=============")
					positionClosed = true
				}
			}
		}
	}
}

// OrderDifference calculate orders arbirtrage
func OrderDifference(bidOrder, askOrder *Order) float64 {
	profit := bidOrder.Price - askOrder.Price
	size := askOrder.Size
	if size > bidOrder.Size {
		size = bidOrder.Size
	}
	return profit * size
}
