package util

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// LoopRequest starts an infinite loop of HTTP requests
func LoopRequest(addr string, seconds time.Duration) <-chan []byte {
	bodyChannel := make(chan []byte)
	dateTicker := time.NewTicker(seconds * time.Second)
	go func() {
		for {
			bodyChannel <- Get(addr)
			<-dateTicker.C
		}
	}()
	fmt.Println("looping API request ", addr, " every ", seconds, " seconds")
	return bodyChannel
}

// Get webpage
func Get(addr string) []byte {
	resp, err := http.Get(addr)
	if err != nil {
		log.Println("ERROR in GET:", err)
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Reading GET HTTP:", err)
		return nil
	}
	return body
}
