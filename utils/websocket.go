package util

import (
	"log"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
)

// WsWriteHeartbeatLoop loop to send heartbeat
func WsWriteHeartbeatLoop(c *websocket.Conn) {
	ticker := time.NewTicker(45 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if err := c.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				log.Println("heartbeat fail:", err)
				return
			}
		}
	}
}

// WsReadLoop loop that reads data a writes to channel
func WsReadLoop(c *websocket.Conn, messageChannel chan<- []byte) {
	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("websocket read fail:", err)
			return
		}
		//log.Printf("recv: %s", message)
		messageChannel <- message
	}
}

// WsWriteMessage write string to  socket
func WsWriteMessage(c *websocket.Conn, msg string) {
	if err := c.WriteMessage(websocket.TextMessage, []byte(msg)); err != nil {
		log.Println("write fail:", err)
		return
	}
}

// WsConnect connect to socket
func WsConnect(host, path string) *websocket.Conn {
	u := url.URL{Scheme: "wss", Host: host, Path: path}

	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}

	return c
}
