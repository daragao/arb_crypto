package util

import (
	"encoding/json"
	"fmt"
)

// ParseJSON generic json parser
func ParseJSON(data string) {
	var x map[string]interface{}
	json.Unmarshal([]byte(data), &x)
	a := x["result"]
	//fmt.Println("%v", a.(map[string]interface{})["XXBTZEUR"].(map[string]interface{})["b"].([]interface{})[1])
	for k, v := range a.(map[string]interface{})["XXBTZEUR"].(map[string]interface{}) {
		fmt.Println(k, v)
	}
}
